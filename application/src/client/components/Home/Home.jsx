import React from 'react'
import {Example} from 'Components'

export default class Home extends React.Component{
    render() {
        return (
            <div>
                Hello world!
                <Example />
            </div>
        )
    }
}