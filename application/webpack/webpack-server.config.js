const path = require('path');

const SRC_DIR = path.resolve("src");
const DIST_DIR_OUTPUT = path.resolve(__dirname, "..", 'dist');

const serverConfig = {
    entry: {
        server: ['babel-polyfill', path.join(SRC_DIR, 'server', 'server.js')]
    },
    output: {
        path: DIST_DIR_OUTPUT,
        filename: '[name].js',
        publicPath: '/dist/',
        pathinfo: true
    },
    target: 'node'
}

module.exports = serverConfig;