const path = require('path');
const webpack = require('webpack');
const baseConfig = require('./webpack.config.js');
const clientConfig = require('./webpack-client.config.js')
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require("html-webpack-plugin");

const SRC_DIR = path.resolve("src");

const devConfig = {
    mode: "development",
    devtool: 'inline-source-map',
    devServer: {
        contentBase: path.resolve(__dirname, "..", 'dist'),
        hot: true,
        port: 9000,
        inline: true,
        host: '0.0.0.0'
    },
    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: path.join(SRC_DIR, 'client', 'index.html')
        }),
        new webpack.EnvironmentPlugin({
            'NODE_ENV': 'development'
        }) 
    ]
}

module.exports = merge(baseConfig, devConfig, clientConfig);