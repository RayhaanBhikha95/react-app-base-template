const path = require('path');

const SRC_DIR = path.resolve("src");
const DIST_DIR_OUTPUT = path.resolve(__dirname, "..", 'dist');

const clientConfig = {
    entry: {
        bundle: ['babel-polyfill', path.join(SRC_DIR, 'client', "index.js")],
    },
    output: {
        path: DIST_DIR_OUTPUT,
        filename: '[name].js',
        publicPath: '/',
        pathinfo: true
    }
}

module.exports = clientConfig;