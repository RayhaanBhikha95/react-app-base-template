const baseConfig = require('./webpack.config.js');
const clientConfig = require('./webpack-client.config.js')
const serverConfig = require('./webpack-server.config.js')
const merge = require('webpack-merge');
const webpack = require("webpack");


const prodConfig = {
    mode: "production",
    plugins: [
        new webpack.EnvironmentPlugin({
            'NODE_ENV': 'production',
        })
    ]
}

module.exports = merge(baseConfig, prodConfig, clientConfig, serverConfig);